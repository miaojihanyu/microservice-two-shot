import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest import models
from hats_rest.models import LocationVO

def get_locations():
    url = "http://wardrobe-api:8000/api/locations"
    response = requests.get(url)
    content = json.loads(response.content) #or we can do content = response.json()
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            defaults={"closet_name": location["closet_name"]}, #a way to say I know we need some data to make this object and if data doesn't exist, use this instead
        )
def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_locations()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
