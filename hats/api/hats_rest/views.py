from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name",]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = { "location": LocationVOEncoder(),}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method=="GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatEncoder,)
    else:
        content = json.loads(request.body) #get the hat object and load it in the content dictionary
        try:
            location_href=content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_show_hat(request, pk):
    if request.method=="DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

# Create your views here.
