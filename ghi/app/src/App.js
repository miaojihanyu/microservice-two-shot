import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import { useState } from "react";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoesList from "./ShoesList";
import ShoesForm from "./ShoesForm";
import HatForm from "./HatForm";
import HatList from "./HatList";

function App(props) {
  const [shoes, setShoes] = useState([]);

  async function getShoes() {
    const response = await fetch("http://localhost:8080/api/shoes/");
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    }
  }

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="shoes">
          <Route
            index
            element={<ShoesList shoes={shoes} getShoes={getShoes} />}
          />
          <Route path="new" element={<ShoesForm />} getShoes={getShoes} />
        </Route>
        <Route path="hats">
          <Route index element={<HatList />} />
          <Route path="new" element={<HatForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
