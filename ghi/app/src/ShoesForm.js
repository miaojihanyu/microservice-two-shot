import React, { useEffect, useState } from "react";

function ShoesForm() {
  const [manufacturer, setManufacturer] = useState("");
  const [modelName, setModelName] = useState("");
  const [color, setColor] = useState("");
  const [picUrl, setPicUrl] = useState("");
  const [wardrobeBin, setWardrobeBin] = useState("");
  const [binList, setBinList] = useState([]);

  const fetchBins = async () => {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBinList(data.bins);
    }
  };
  useEffect(() => {
    fetchBins();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      manufacturer,
      model_name: modelName,
      color,
      pic_url: picUrl,
      wardrobe_bin: wardrobeBin,
    };

    const shoesUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoes = await response.json();
      console.log(newShoes);
      setManufacturer("");
      setModelName("");
      setColor("");
      setPicUrl("");
      setWardrobeBin("");
    }
  };
  function handleManufacturerChange(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  function handleModelNameChange(event) {
    const { value } = event.target;
    setModelName(value);
  }

  function handleColorChange(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handlePicUrlChange(event) {
    const { value } = event.target;
    setPicUrl(value);
  }

  function handleWardrobeBinChange(event) {
    const { value } = event.target;
    setWardrobeBin(value);
  }
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create New Shoe</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleManufacturerChange}
                    placeholder="Manufacturer"
                    required
                    type="text"
                    name="manufacturer"
                    id="manufacturer"
                    className="form-control"
                    value={manufacturer}
                  />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleModelNameChange}
                    placeholder="Model name"
                    required
                    type="text"
                    name="modelName"
                    id="modelName"
                    className="form-control"
                    value={modelName}
                  />
                  <label htmlFor="modelName">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleColorChange}
                    placeholder="Color"
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                    value={color}
                  />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handlePicUrlChange}
                    placeholder="Picture URL"
                    required
                    type="url"
                    name="pic_url"
                    id="pic_url"
                    className="form-control"
                    value={picUrl}
                  />
                  <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="form-floating mb-3">
                  <select
                    onChange={handleWardrobeBinChange}
                    placeholder="wardrobeBin"
                    required
                    className="form-select"
                    id="wardrobeBin"
                    value={wardrobeBin}
                  >
                    <option value="">Choose a Bin</option>
                    {binList.map((bin) => {
                      return (
                        <option key={bin.href} value={bin.href}>
                          {bin.closet_name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ShoesForm;
