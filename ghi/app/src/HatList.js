import {useEffect, useState } from 'react';

function HatList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/hats/");

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }
    const deleteHat = async function(hatToDelete) {
        const response = await fetch (`http://localhost:8090/api/hats/${hatToDelete}/`, {
            method: "DELETE",
        });
        if (response.ok) {
            getData()
        } else {
          console.log(response)
        }


    }



    useEffect(() => {
        getData()
    }, [])

    return (
        <table className="table table-">
        <thead className="thead-dark">
          <tr>
            <th>Delete</th>
            <th>Hat Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (

              <tr key={hat.id}>
              <td><div><button onClick={() => deleteHat(hat.id)}>Delete</button></div></td>
                <td>{ hat.style_name }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location.closet_name }</td>
                <td className="w-25 p-3" ><div><img src={hat.picture_url} className="img-fluid" alt="Responsive image" /></div></td>
              </tr>


            );
          })}
        </tbody>
      </table>
    )
}

export default HatList;
