from django.db import models
from django.urls import reverse

# Create your models here.


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=100, unique=True)


def get_api_url(self):
    return reverse("api_bin", kwargs={"pk": self.pk})


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    pic_url = models.URLField(null=True)
    wardrobe_bin = models.ForeignKey(
        "BinVO", related_name="bin", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.model_name
