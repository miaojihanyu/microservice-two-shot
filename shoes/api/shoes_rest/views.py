from django.shortcuts import render

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoes, BinVO


# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoesEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "pic_url",
        "wardrobe_bin",
        "id",
    ]
    encoders = {"wardrobe_bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["wardrobe_bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["wardrobe_bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400,
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_shoes_detail(request, id):
    if request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})
