from django.urls import path
from shoes_rest.views import api_list_shoes, api_shoes_detail


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:id>/", api_shoes_detail, name="api_shoes_detail"),
]
